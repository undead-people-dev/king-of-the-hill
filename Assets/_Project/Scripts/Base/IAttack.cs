public interface IAttack
{
    bool StartLightAttack();
    void StopLightAttack();
    
    bool StartHeavyAttack();
    void StopHeavyAttack();
}