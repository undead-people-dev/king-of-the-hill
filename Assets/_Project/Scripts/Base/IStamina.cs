public interface IStamina
{
    float MaxStamina { get; }
    float Stamina { get; }

    bool Consume(float stamina);
}