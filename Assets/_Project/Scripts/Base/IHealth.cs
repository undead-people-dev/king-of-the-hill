using UnityEngine;

public interface IHealth
{
    
    float MaxHealth { get; }
    float Health { get; }

    void DealDamage(float damage, GameObject attacker);

}