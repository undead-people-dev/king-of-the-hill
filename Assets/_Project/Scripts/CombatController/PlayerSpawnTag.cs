﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnTag : MonoBehaviour
{

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position + Vector3.up, new Vector3(1f, 2f, 1f));
    }
}
