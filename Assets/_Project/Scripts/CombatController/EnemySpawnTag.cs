﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnTag : MonoBehaviour
{

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position + Vector3.up, new Vector3(1f, 2f, 1f));
    }
}
