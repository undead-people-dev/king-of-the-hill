﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CombatController : MonoBehaviour
{
    
    public delegate void StartGameHandler();
    public delegate void WinHandler();
    public delegate void DefeatHandler();
    
    public event StartGameHandler OnStartGame;
    public event WinHandler OnWin;
    public event DefeatHandler OnDefeat;

    [Header("Enemy")]
    [SerializeField] private List<EnemyData> _enemies = default;
    [Header("Player")]
    [SerializeField] private GameObject _playerPrefab = default;

    public List<EnemyData> Enemies => _enemies;

    private Transform _enemySpawnPoint;
    private Transform _playerSpawnPoint;
    private Vector3 _arenaCentralPoint;

    private int _enemiesAlive = 0;

    private void Start()
    {
        StartCoroutine(StartGameCoroutine());
    }

    private IEnumerator StartGameCoroutine()
    {
        GetSpawns();
        SpawnPlayer();
        SpawnEnemies();

        yield return new WaitForSeconds(3f);

        StateMachine[] states = GameObject.FindObjectsOfType<StateMachine>();
        foreach (var item in states)
        {
            item.ChangeState(StateType.Move);
        }

        OnStartGame?.Invoke();
    }

    private void SpawnPlayer()
    {
        Vector3 centerPos = new Vector3(0f, _playerSpawnPoint.position.y, 0f);
        GameObject playerCreated = Instantiate(_playerPrefab, _playerSpawnPoint.position, Quaternion.identity);
        playerCreated.transform.LookAt(_arenaCentralPoint);

		Camera mainCamera = Camera.main;
        CinemachineVirtualCamera cinemachine = mainCamera.transform.parent.GetComponentInChildren<CinemachineVirtualCamera>();
        cinemachine.Follow = playerCreated.transform;
        cinemachine.LookAt = playerCreated.transform;

        playerCreated.GetComponent<EntityHealth>().OnDeath += HandlePlayerDeath;
    }

    private void SpawnEnemies()
    {
        Vector3 centerPos = new Vector3(0f, _playerSpawnPoint.position.y, 0f);
        for (int i = 0; i < _enemies.Count; i++)
        {
            GameObject prefab = _enemies[i].Prefab;
            Vector3 spawnPosition = _enemySpawnPoint.position;

            spawnPosition += i * _enemySpawnPoint.right * (i % 2 == 0 ? 1f : -1f);

            GameObject enemyCreated = Instantiate(prefab, spawnPosition, Quaternion.identity);
            enemyCreated.GetComponent<EntityHealth>().OnDeath += HandleEnemyDeath;
            enemyCreated.transform.LookAt(_arenaCentralPoint);
            _enemiesAlive += 1;
        }
    }

    private void GetSpawns()
    {
        _enemySpawnPoint = GameObject.FindObjectOfType<EnemySpawnTag>().transform;
        _playerSpawnPoint = GameObject.FindObjectOfType<PlayerSpawnTag>().transform;
        
        _arenaCentralPoint = new Vector3(0f, _playerSpawnPoint.position.y, 0f);
    }

    private void HandleEnemyDeath()
    {
        _enemiesAlive -= 1;
        if (_enemiesAlive <= 0)
        {
            Win();
        }
    }

    private void HandlePlayerDeath()
    {
        OnDefeat?.Invoke();
    }

    private void Win()
    {
        Inventory.Main.AddGold(GoldReward());
        Inventory.Main.AddFame(FameReward());
        OnWin?.Invoke();
    }

    public int GoldReward()
    {
        int gold = 0;
        gold = _enemies.Sum(x => x.Gold);
        if (_enemies.Count > 1)
        {
            gold += Mathf.RoundToInt(gold * (_enemies.Count * .2f));
        }
        return gold;
    }

    public int FameReward()
    {
        int fame = 0;
        fame = _enemies.Sum(x => x.Fame);
        if (_enemies.Count > 1)
        {
            fame += Mathf.RoundToInt(fame * (_enemies.Count * .25f));
        }
        return fame;
    }

}
