﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Initializer
{
    
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void InitializeOnLoad()
    {

        Debug.Log("== Running Initializer!");

        EquipmentManager.Load();
        LevelManager.Load();
        EnemiesManager.Load();

        Inventory inv = new Inventory();
        
        foreach (var item in EquipmentManager.Equipments)
        {
            inv.AddEquipment(item);
        }
        
        foreach (var item in EquipmentManager.Weapons)
        {
            inv.AddWeapon(item);
        }

        Debug.Log("== Finished initializing game data!");

    }

}
