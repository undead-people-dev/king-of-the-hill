﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FloatingText : MonoBehaviour
{
    
    private TextMeshPro _textMesh = default;

    private float _acceleration;
    private float _speed;
    private Vector3 _direction;

    public void Construct(string text, Color color, float speed = 2f, float slowdown = 0f, float lifeTime = 2f)
    {
        Construct(text, color, speed, Vector3.up, slowdown, lifeTime);
    }

    public void Construct(string text, Color color, float speed, Vector3 direction, float slowdown, float lifeTime)
    {
        _textMesh.text = text;
        _textMesh.color = color;
        _speed = speed;
        _direction = direction;
        _acceleration = -slowdown;
        StartCoroutine(LifeCoroutine(lifeTime));
    }

    void Awake()
    {
        _textMesh = GetComponentInChildren<TextMeshPro>();
    }

    void Update()
    {
        transform.Translate(_speed * _direction * Time.unscaledDeltaTime);
        _speed += _acceleration * Time.unscaledDeltaTime;
    }

    IEnumerator LifeCoroutine(float lifeTime)
    {
        yield return new WaitForSecondsRealtime(lifeTime);
        Destroy(gameObject);
    }

}
