public class CameraShakePreset
{

    public static CameraShakePreset Attack = new CameraShakePreset(0f, 1.7f, 1.3f, .1f, .5f);
    public static CameraShakePreset HeavyAttack = new CameraShakePreset(0f, 3f, 2.5f, .1f, .5f);
    public static CameraShakePreset Damage = new CameraShakePreset(0f, 2f, 2.0f, .1f, .5f);

    private float m_Duration;
    public float Duration { get => m_Duration; }

    private float m_Amplitude;
    public float Amplitude { get => m_Amplitude; }

    private float m_Frequency;
    public float Frequency { get => m_Frequency; }

    private float m_FadeIn;
    public float FadeIn { get => m_FadeIn; }

    private float m_FadeOut;
    public float FadeOut { get => m_FadeOut; }

    public CameraShakePreset(float duration, float amplitude, float frequency, float fadeIn, float fadeOut)
    {
        m_Duration = duration;
        m_Amplitude = amplitude;
        m_Frequency = frequency;
        m_FadeIn = fadeIn;
        m_FadeOut = fadeOut;
    }
}