﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShake : MonoBehaviour
{

    public static CameraShake Instance;

    private const float _fadeRate = 0.05f;
    private bool _isShaking = false;

    // Cinemachine Shake
    private CinemachineVirtualCamera _virtualCamera;
    private CinemachineBasicMultiChannelPerlin _virtualCameraNoise;

    // Use this for initialization
    void Awake()
    {
        Instance = this;

        // Get Virtual Camera Noise Profile
        _virtualCamera = GetComponentInChildren<CinemachineVirtualCamera>();
        _virtualCameraNoise = _virtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        _virtualCameraNoise.m_AmplitudeGain = 0f;
    }

    public void Shake(float duration, float amplitude, float frequency, float fadeIn = 0f, float fadeOut = 0f)
    {
        Shake(new CameraShakePreset(duration, amplitude, frequency, fadeIn, fadeOut));
    }

    public void Shake(CameraShakePreset preset)
    {
        if (_isShaking) return;
        _isShaking = true;
        StartCoroutine(ShakeCoroutine(preset));
    }

    IEnumerator ShakeCoroutine(CameraShakePreset preset)
    {
        float startTime = Time.time;
        if (preset.FadeIn > 0f)        
        {
            yield return ShakeFade(preset.FadeIn, preset.Amplitude, preset.Frequency);
        }

        // Set Cinemachine Camera Noise parameters
        _virtualCameraNoise.m_AmplitudeGain = preset.Amplitude;
        _virtualCameraNoise.m_FrequencyGain = preset.Frequency;
        
        yield return new WaitForSeconds(preset.Duration);
        startTime = Time.time;
        if (preset.FadeOut > 0f)            
        {
            yield return ShakeFade(preset.FadeOut, 0f, 0f);
        }

        _virtualCameraNoise.m_AmplitudeGain = 0f;
        _isShaking = false;

    }

    IEnumerator ShakeFade(float duration, float amplitude, float frequency)
    {
        float perc = 0f;
        float durationRate = 1f / duration;
        while (perc < 1f)
        {
            _virtualCameraNoise.m_AmplitudeGain = Mathf.Lerp(_virtualCameraNoise.m_AmplitudeGain, amplitude, perc);
            _virtualCameraNoise.m_FrequencyGain = Mathf.Lerp(_virtualCameraNoise.m_FrequencyGain, frequency, perc);
            perc += _fadeRate * durationRate;
            yield return new WaitForSeconds(_fadeRate);
        }
    }

}
