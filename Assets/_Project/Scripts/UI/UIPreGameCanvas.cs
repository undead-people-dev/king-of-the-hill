﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIPreGameCanvas : MonoBehaviour
{
    
    [SerializeField] private TextMeshProUGUI _playerNameText = default;
    [SerializeField] private TextMeshProUGUI _enemyNameText = default;

    private void OnEnable()
    {
        CombatController controller = GameObject.FindObjectOfType<CombatController>();
        _playerNameText.text = "Player".ToUpper();
        string enemyName = "";
        foreach (var item in controller.Enemies)
        {
            enemyName += item.name + "\n";
        }
        _enemyNameText.text = enemyName.ToUpper();
    }

}
