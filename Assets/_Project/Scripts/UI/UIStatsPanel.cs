﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIStatsPanel : MonoBehaviour
{
    
    [SerializeField] private TextMeshProUGUI _statsText = default;
    [SerializeField] private Image _headIcon = default;
    [SerializeField] private Image _bodyIcon = default;
    [SerializeField] private Image _ringIcon = default;

    private void Awake()
    {
        PlayerHealth playerHealth = GameObject.FindObjectOfType<PlayerHealth>();
        EntityStatsHolder statsHolder = playerHealth.GetComponent<EntityStatsHolder>();

        Stats diffStats = statsHolder.Stats - statsHolder.BaseStats;

        _statsText.text = "";
        _statsText.text += SetText("Gladiator", statsHolder.Stats.Gladiator, statsHolder.BaseStats.Gladiator) + "\n";
        _statsText.text += SetText("Marksman", statsHolder.Stats.Marksman, statsHolder.BaseStats.Marksman) + "\n";
        _statsText.text += SetText("Athlete", statsHolder.Stats.Athlete, statsHolder.BaseStats.Athlete) + "\n";
        _statsText.text += SetText("Acrobat", statsHolder.Stats.Acrobat, statsHolder.BaseStats.Acrobat) + "\n";
        _statsText.text += SetText("Warrior", statsHolder.Stats.Warrior, statsHolder.BaseStats.Warrior) + "\n";
        _statsText.text += SetText("Performer", statsHolder.Stats.Performer, statsHolder.BaseStats.Performer);

        if (statsHolder.Head)
        {
            _headIcon.sprite = statsHolder.Head.Icon;
        }
        else
        {
            _headIcon.gameObject.SetActive(false);
        }
        
        if (statsHolder.Body)
        {
            _bodyIcon.sprite = statsHolder.Body.Icon;
        }
        else
        {
            _bodyIcon.gameObject.SetActive(false);
        }
        
        if (statsHolder.Ring)
        {
            _ringIcon.sprite = statsHolder.Ring.Icon;
        }
        else
        {
            _ringIcon.gameObject.SetActive(false);
        }
        
    
    }

    private string SetText(string name, int stat, int baseStat)
    {
        int diff = stat - baseStat;
        string color = "";
        if (diff >= 0) color = "<color=#037d50>";
        else color = "<color=red>";

        return string.Format("{0}: {1} ({2} {3}{4:+#;-#;0}</color>)", name, stat, baseStat, color, diff);
    }

}
