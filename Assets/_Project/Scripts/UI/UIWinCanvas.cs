﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIWinCanvas : MonoBehaviour
{
    
    [SerializeField] private TextMeshProUGUI _goldText = default;
    [SerializeField] private TextMeshProUGUI _fameText = default;

    private CombatController _controller;

    private void OnEnable()
    {
        _controller = GameObject.FindObjectOfType<CombatController>();
        StartCoroutine(WinCoroutine());
    }

    private IEnumerator WinCoroutine()
    {
        int goldEarned = _controller.GoldReward();
        int fameEarned = _controller.FameReward();

        int goldAmount = Inventory.Main.Gold - goldEarned;
        int fameAmount = Inventory.Main.Fame - fameEarned;
        
        _goldText.text = goldAmount.ToString();
        _fameText.text = fameAmount.ToString();

        yield return new WaitForSeconds(1f);

        yield return StartCoroutine(IncreaseNumberTextCoroutine(_goldText, goldAmount, goldEarned));

        yield return new WaitForSeconds(.5f);
        _goldText.text = string.Format("{0}", Inventory.Main.Gold);

        yield return new WaitForSeconds(1f);

        yield return StartCoroutine(IncreaseNumberTextCoroutine(_fameText, fameAmount, fameEarned));

        yield return new WaitForSeconds(.5f);
        _fameText.text = string.Format("{0}", Inventory.Main.Fame);
        
        yield return new WaitForSeconds(5f);
        LevelManager.LoadInventory();
    }

    private IEnumerator IncreaseNumberTextCoroutine(TextMeshProUGUI text, int from, int increase, float time = 1f)
    {
        float timeBetween = time / increase;
        timeBetween = Mathf.Max(timeBetween, Time.deltaTime * 2f);
        
        int valueIncrease = Mathf.CeilToInt(increase * timeBetween);
        for (int i = 1; i <= increase; i += valueIncrease)
        {
            text.text = string.Format("{0} + {1}", from, i);
            yield return new WaitForSeconds(timeBetween);
        }
        
        text.text = string.Format("{0} + {1}", from, increase);
    }

}
