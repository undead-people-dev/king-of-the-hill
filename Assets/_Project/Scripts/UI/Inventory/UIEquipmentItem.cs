﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEquipmentItem : MonoBehaviour
{

    [SerializeField] private Image _icon = default;

    private Button _button;

    protected Equipment _equipment;

    public void Construct(Equipment equipment)
    {
        _equipment = equipment;
        _icon.sprite = _equipment.Icon;
        
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() => _equipment.Equip());
    }

}
