﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIInventory : MonoBehaviour
{
    
    [Header("Equip")]
    [SerializeField] private UIEquipmentItem _headEquip = default;
    [SerializeField] private UIEquipmentItem _bodyEquip = default;
    [SerializeField] private UIEquipmentItem _ringEquip = default;
    [SerializeField] private UIWeaponItem _meleeWeaponEquip = default;
    [SerializeField] private UIWeaponItem _rangeWeaponEquip = default;
    [SerializeField] private TextMeshProUGUI _statsText = default;

    [Header("Weapons")]
    [SerializeField] private Transform _weaponsParent = default;
    [SerializeField] private GameObject _weaponItemPrefab = default;

    [Header("Equipments")]
    [SerializeField] private Transform _equipmentsParent = default;
    [SerializeField] private GameObject _equipmentItemPrefab = default;
    
    [Header("Values")]
    [SerializeField] private TextMeshProUGUI _goldValue = default;
    [SerializeField] private TextMeshProUGUI _fameValue = default;

    [Header("Button")]
    [SerializeField] private Button _playButton = default;

    private void Start()
    {

        foreach (var item in Inventory.Main.Weapons)
        {
            GameObject created = Instantiate(_weaponItemPrefab, _weaponsParent);
            created.GetComponent<UIWeaponItem>().Construct(item);
        }

        foreach (var item in Inventory.Main.Equipments)
        {
            GameObject created = Instantiate(_equipmentItemPrefab, _equipmentsParent);
            created.GetComponent<UIEquipmentItem>().Construct(item);
        }

        _goldValue.text = Inventory.Main.Gold.ToString();
        _fameValue.text = Inventory.Main.Fame.ToString();

        HandleChangeEquipment();
    }

    private void OnEnable()
    {
        _playButton.onClick.AddListener(HandlePlayClick);
        Inventory.Main.OnChangeEquipment += HandleChangeEquipment;
    }

    private void OnDisable()
    {
        _playButton.onClick.RemoveListener(HandlePlayClick);
        Inventory.Main.OnChangeEquipment -= HandleChangeEquipment;
    }

    private void HandlePlayClick()
    {
        LevelManager.LoadGame();
    }

    private void HandleChangeEquipment()
    {
        Stats equipmentStats = new Stats();

        if (Inventory.Main.Head != null)
        {
            _headEquip.gameObject.SetActive(true);
            _headEquip.Construct(Inventory.Main.Head);

            equipmentStats += Inventory.Main.Head.Stats;
        }
        else
        {
            _headEquip.gameObject.SetActive(false);
        }

        if (Inventory.Main.Body != null)
        {
            _bodyEquip.gameObject.SetActive(true);
            _bodyEquip.Construct(Inventory.Main.Body);

            equipmentStats += Inventory.Main.Body.Stats;
        }
        else
        {
            _bodyEquip.gameObject.SetActive(false);
        }

        if (Inventory.Main.Ring != null)
        {
            _ringEquip.gameObject.SetActive(true);
            _ringEquip.Construct(Inventory.Main.Ring);

            equipmentStats += Inventory.Main.Ring.Stats;
        }
        else
        {
            _ringEquip.gameObject.SetActive(false);
        }

        if (Inventory.Main.MeleeWeapon != null)
        {
            _meleeWeaponEquip.gameObject.SetActive(true);
            _meleeWeaponEquip.Construct(Inventory.Main.MeleeWeapon);
        }
        else
        {
            _meleeWeaponEquip.gameObject.SetActive(false);
        }

        if (Inventory.Main.RangeWeapon != null)
        {
            _rangeWeaponEquip.gameObject.SetActive(true);
            _rangeWeaponEquip.Construct(Inventory.Main.RangeWeapon);
        }
        else
        {
            _rangeWeaponEquip.gameObject.SetActive(false);
        }

        Stats baseStats = Inventory.Main.BaseStats;
        _statsText.text = "";
        _statsText.text += string.Format("Gladiator {0} ({1})", baseStats.Gladiator, equipmentStats.Gladiator) + "\n";
        _statsText.text += string.Format("Marksman {0} ({1})", baseStats.Marksman, equipmentStats.Marksman) + "\n";
        _statsText.text += string.Format("Athlete {0} ({1})", baseStats.Athlete, equipmentStats.Athlete) + "\n";
        _statsText.text += string.Format("Acrobat {0} ({1})", baseStats.Acrobat, equipmentStats.Acrobat) + "\n";
        _statsText.text += string.Format("Warrior {0} ({1})", baseStats.Warrior, equipmentStats.Warrior) + "\n";
        _statsText.text += string.Format("Performer {0} ({1})", baseStats.Performer, equipmentStats.Performer);
    }

}
