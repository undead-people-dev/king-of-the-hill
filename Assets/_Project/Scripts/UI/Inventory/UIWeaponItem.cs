﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWeaponItem : MonoBehaviour
{

    [SerializeField] private Image _icon = default;

    private Button _button;
    protected Weapon _weapon;

    public void Construct(Weapon weapon)
    {
        _weapon = weapon;
        _icon.sprite = _weapon.Icon;
        
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() => _weapon.Equip());
    }

}
