﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameCanvas : MonoBehaviour
{
    [SerializeField] private GameObject _preGameCanvas = default;
    [SerializeField] private GameObject _inGameCanvas = default;
    [SerializeField] private GameObject _winCanvas = default;
    [SerializeField] private GameObject _defeatCanvas = default;

    private CombatController _controller;

    private void Awake()
    {
        _controller = GameObject.FindObjectOfType<CombatController>();
    }

    private void OnEnable()
    {
        HandleStartScene();
        _controller.OnStartGame += HandleStartGame;
        _controller.OnWin += HandleWin;
        _controller.OnDefeat += HandleDefeat;
    }

    private void OnDisable()
    {
        _controller.OnStartGame -= HandleStartGame;
        _controller.OnWin -= HandleWin;
        _controller.OnDefeat -= HandleDefeat;
    }
    
    private void HandleStartScene()
    {
        _preGameCanvas.SetActive(true);
        _inGameCanvas.SetActive(false);
        _winCanvas.SetActive(false);
        _defeatCanvas.SetActive(false);
    }
    
    private void HandleStartGame()
    {
        _preGameCanvas.SetActive(false);
        _inGameCanvas.SetActive(true);
        _winCanvas.SetActive(false);
        _defeatCanvas.SetActive(false);
    }
    
    private void HandleWin()
    {
        _preGameCanvas.SetActive(false);
        _inGameCanvas.SetActive(false);
        _winCanvas.SetActive(true);
        _defeatCanvas.SetActive(false);
    }
    
    private void HandleDefeat()
    {
        _preGameCanvas.SetActive(false);
        _inGameCanvas.SetActive(false);
        _winCanvas.SetActive(false);
        _defeatCanvas.SetActive(true);
    }
}
