using UnityEngine;

public class DamageCalculator
{

    public static float HeavyMeleeAttack(float damage, GameObject attacker, GameObject target)
    {
        return HeavyMeleeAttack(damage, attacker.GetComponent<EntityStatsHolder>(), target.GetComponent<EntityStatsHolder>());
    }

    public static float HeavyMeleeAttack(float damage, EntityStatsHolder attackerStats, EntityStatsHolder targetStats)
    {
        float realDamage = damage + attackerStats.HeavyAttackDamage();
        
        if (realDamage < 0) return 0f;
        return realDamage;
    }

    public static float LightMeleeAttack(float damage, GameObject attacker, GameObject target)
    {
        return LightMeleeAttack(damage, attacker.GetComponent<EntityStatsHolder>(), target.GetComponent<EntityStatsHolder>());
    }

    public static float LightMeleeAttack(float damage, EntityStatsHolder attackerStats, EntityStatsHolder targetStats)
    {
        float realDamage = damage + attackerStats.LightAttackDamage();
        
        if (realDamage < 0) return 0f;
        return realDamage;
    }

    public static float RangeAttack(Projectile projectile, GameObject attacker, GameObject target)
    {
        return RangeAttack(projectile, attacker.GetComponent<EntityStatsHolder>(), target.GetComponent<EntityStatsHolder>());
    }

    public static float RangeAttack(Projectile projectile, EntityStatsHolder attackerStats, EntityStatsHolder targetStats)
    {
        float realDamage = projectile.Damage + attackerStats.RangeAttackDamage();
        
        if (realDamage < 0) return 0f;
        return realDamage;
    }

}