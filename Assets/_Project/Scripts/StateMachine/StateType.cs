public enum StateType
{
    None,
    Initial,
    Move,
    Roll,
    LightAttack,
    HeavyAttack,
    Death
}