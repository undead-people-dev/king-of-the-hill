﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseState
{

    protected StateMachine _stateMachine;
    protected GameObject _gameObject;

    public BaseState(GameObject gameObject)
    {
        _gameObject = gameObject;
        _stateMachine = gameObject.GetComponent<StateMachine>();   
    }

    public abstract void Start();
    public abstract void Stop();
    public abstract void Tick();

}
