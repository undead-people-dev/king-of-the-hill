using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityDeathState : BaseState
{    

    private Animator _animator;

    public EntityDeathState(GameObject gameObject) : base(gameObject)
    {
        _animator = gameObject.GetComponentInChildren<Animator>();
    }

    public override void Start()
    {
        _animator.applyRootMotion = true;
        _animator.SetTrigger("Death");
    }

    public override void Stop()
    {

    }

    public override void Tick()
    {
    }

}