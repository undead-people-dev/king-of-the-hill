using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityWaitingState : BaseState
{    

    public EntityWaitingState(GameObject gameObject) : base(gameObject)
    {
    }

    public override void Start()
    {
        
    }

    public override void Stop()
    {

    }

    public override void Tick()
    {
    }

}