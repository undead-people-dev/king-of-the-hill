﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    
    public delegate void ChangeStateHandler(StateType type, BaseState state);
    
    public event ChangeStateHandler OnChangeState;

    private Dictionary<StateType, BaseState> _states;

    private BaseState _currentState;

    public void Construct(Dictionary<StateType, BaseState> states)
    {
        _states = states;
        _currentState = _states.Values.First();
        _currentState.Start();
    }

    public void ChangeState(StateType nextState, BaseState state)
    {
        if (_states.ContainsKey(nextState)) 
        {
            _states[nextState] = state;
            ChangeState(nextState);
            return;
        }

        _states.Add(nextState, state);
        ChangeState(nextState);
    }

    public void ChangeState(StateType nextState)
    {
        if (!_states.ContainsKey(nextState)) return;

        _currentState.Stop();
        _currentState = _states[nextState];
        _currentState.Start();

        OnChangeState?.Invoke(nextState, _currentState);
    }

    private void Update()
    {
        if (_states == null) return;

        _currentState.Tick();
    }

}
