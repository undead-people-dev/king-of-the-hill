﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EnemiesManager
{

    const string ALL_ENEMIES = "Enemies";

    private static List<EnemyData> _enemies;

    public static void Load()
    {

        _enemies = new List<EnemyData>();

        foreach (var item in Resources.LoadAll<EnemyData>(ALL_ENEMIES))
        {
            _enemies.Add(item);
        }

    }

    public static List<EnemyData> GetEnemies(int amount, int fame)
    {
        return _enemies;
    }

}
