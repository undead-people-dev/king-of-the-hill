﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class LevelManager
{

    const string GAME_SCENE = "GameArena";
    const string INVENTORY_SCENE = "Inventory";
    
    public static void Load()
    {

    }

    public static void LoadGame()
    {
        SceneManager.LoadScene(GAME_SCENE);
    }

    public static void LoadInventory()
    {
        SceneManager.LoadScene(INVENTORY_SCENE);
    }
    
}
