﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EquipmentManager
{
    
    const string ALL_MELEE_WEAPONS = "MeleeWeapons";
    const string ALL_RANGE_WEAPONS = "RangeWeapons";
    const string ALL_EQUIPMENTS = "Equipments";

    private static List<Weapon> _weapons;
    public static List<Weapon> Weapons => _weapons;

    private static List<Equipment> _equipments;
    public static List<Equipment> Equipments => _equipments;

    public static void Load()
    {

        _equipments = new List<Equipment>();
        _weapons = new List<Weapon>();
        
        foreach (var item in Resources.LoadAll<Equipment>(ALL_EQUIPMENTS))
        {
            _equipments.Add(item);
        }
        
        foreach (var item in Resources.LoadAll<Weapon>(ALL_MELEE_WEAPONS))
        {
            _weapons.Add(item);
        }
        
        foreach (var item in Resources.LoadAll<Weapon>(ALL_RANGE_WEAPONS))
        {
            _weapons.Add(item);
        }
    }

}