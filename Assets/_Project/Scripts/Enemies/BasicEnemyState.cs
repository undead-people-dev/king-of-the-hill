﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemyState : MonoBehaviour
{
    
    private StateMachine _stateMachine;

    private void Awake()
    {
        EntityAttack atack = GetComponent<EntityAttack>();
        EntityHealth health = GetComponent<EntityHealth>();
        CombatController controller = GameObject.FindObjectOfType<CombatController>();
        
        _stateMachine = GetComponent<StateMachine>();
        _stateMachine.Construct(new Dictionary<StateType, BaseState>() {
            { StateType.Initial, new EntityWaitingState(gameObject) },
            { StateType.Move, new EnemyFollowPlayerState(gameObject) },
            { StateType.LightAttack, new EnemyLightAttackState(atack, gameObject) },
            { StateType.Death, new EntityDeathState(gameObject) },
        });

        health.OnDeath += () => _stateMachine.ChangeState(StateType.Death);
        controller.OnDefeat += () => _stateMachine.ChangeState(StateType.Initial);
    }

}
