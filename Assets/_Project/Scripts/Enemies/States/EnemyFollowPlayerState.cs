﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyFollowPlayerState : EnemyBaseState
{

    protected NavMeshAgent _agent;
    protected Animator _animator;
    protected EntityStatsHolder _stats;

    public EnemyFollowPlayerState(GameObject gameObject) : base(gameObject)
    {
        _agent = gameObject.GetComponent<NavMeshAgent>();
        _animator = gameObject.GetComponentInChildren<Animator>();
        _stats = gameObject.GetComponent<EntityStatsHolder>();
    }

    public override void Start()
    {
        float multiplierSpeed = _stats.MovementSpeedMultiplier();
        _agent.speed = multiplierSpeed * _agent.speed;
        _agent.angularSpeed = multiplierSpeed * _agent.angularSpeed;
        _animator.SetFloat("MoveSpeedMultipler", multiplierSpeed);
    }

    public override void Stop()
    {
        _animator.SetFloat("SpeedX", 0f);
        _animator.SetFloat("SpeedZ", 0f);
    }

    public override void Tick()
    {
        if (Vector3.Distance(_player.position, _gameObject.transform.position) <= _agent.stoppingDistance)
        {
            _stateMachine.ChangeState(StateType.LightAttack);
            return;
        }
        
        _agent.SetDestination(_player.position);
        
        Vector3 localVelocity = _gameObject.transform.InverseTransformDirection(_agent.velocity);
        _animator.SetFloat("SpeedX", localVelocity.x / 7f);
        _animator.SetFloat("SpeedZ", localVelocity.z / 7f);
    }
    
}
