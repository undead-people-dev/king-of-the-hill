﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyLightAttackState : EnemyBaseState
{    

    private EntityAttack _attack;
    protected NavMeshAgent _agent;

    public EnemyLightAttackState(EntityAttack attack, GameObject gameObject) : base(gameObject)
    {
        _agent = gameObject.GetComponent<NavMeshAgent>();
        _attack = attack;
    }

    public override void Start()
    {
        if (!_attack.StartLightAttack())
        {
            _stateMachine.ChangeState(StateType.Move);
            return;
        }
        
        _attack.OnEndAttack += HandleEndAttack;
    }

    public override void Stop()
    {
        _attack.StopLightAttack();
        _attack.StopAllCoroutines();
        _attack.OnEndAttack -= HandleEndAttack;
    }

    public override void Tick()
    {
        _attack.StopLightAttack();

        RotateTowardsTarget();
    }

    private void HandleEndAttack()
    {
        if (Vector3.Distance(_player.position, _gameObject.transform.position) < _agent.stoppingDistance)
        {
            _attack.StartCoroutine(WaitForAttackCoroutine());
        }
        else
        {
            _stateMachine.ChangeState(StateType.Move);
        }
    }

    private void RotateTowardsTarget ()
    {
        _agent.enabled = false;
        Vector3 direction = (_player.transform.position - _gameObject.transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        _gameObject.transform.rotation = lookRotation;
        _agent.enabled = true;
        // _gameObject.transform.rotation = Quaternion.Slerp(_gameObject.transform.rotation, lookRotation, Time.deltaTime * _agent.angularSpeed);
    }

    private IEnumerator WaitForAttackCoroutine()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(.5f, 2f));
        if (!_attack.StartLightAttack())
        {
            _stateMachine.ChangeState(StateType.Move);
        }
    }

}