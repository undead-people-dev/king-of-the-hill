﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyBaseState : BaseState
{
    
    protected Transform _player;

    public EnemyBaseState(GameObject gameObject) : base(gameObject)
    {
        _player = GameObject.FindObjectOfType<PlayerState>().transform;
    }

}
