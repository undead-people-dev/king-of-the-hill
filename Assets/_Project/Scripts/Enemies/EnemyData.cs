﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/New Enemy")]
public class EnemyData : ScriptableObject
{
    
    [Header("Info")]
    [SerializeField] private string _name = default;
    public string Name => _name;

    [SerializeField] private int _tier = default;
    public int Tier => _tier;

    [SerializeField] private GameObject _prefab = default;
    public GameObject Prefab => _prefab;
    
    [Header("Reward")]
    [SerializeField] private int _gold = default;
    public int Gold => _gold;

    [SerializeField] private int _fame = default;
    public int Fame => _fame;

}
