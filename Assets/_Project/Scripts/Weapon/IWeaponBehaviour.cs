public interface IWeaponBehaviour
{
    
    void Construct(Weapon weapon);
    bool UseLightAttack(EntityStamina stamina);
    bool UseHeavyAttack(EntityStamina stamina);

}