﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponDamageDealer : MonoBehaviour, IWeaponBehaviour
{

    [Header("Light")]
    [SerializeField] protected float _minLightDamage = 10f;
    [SerializeField] protected float _maxLightDamage = 10f;
    [SerializeField] protected float _staminaConsume = 0f;

    public float LightDamage => Random.Range(_minLightDamage, _maxLightDamage);

    [Header("Heavy")]
    [SerializeField] protected float _minHeavyDamage = 10f;
    [SerializeField] protected float _maxHeavyDamage = 10f;
    [SerializeField] protected float _heavyStaminaConsume = 35f;

    public float HeavyDamage => Random.Range(_minHeavyDamage, _maxHeavyDamage);

    protected GameObject _entity;
    protected EntityStatsHolder _stats;
    protected Collider[] _colliders;

    protected EntityAttack _entityAttack;

    protected bool _isHeavyAttack;

    protected List<GameObject> _objectsDamaged;
    
    private void Awake()
    {
        _entityAttack = GetComponentInParent<EntityAttack>();
        _entity = _entityAttack.gameObject;
        _stats = _entity.GetComponent<EntityStatsHolder>();

        _colliders = GetComponentsInChildren<Collider>();
        EnableColliders(false);

        _objectsDamaged = new List<GameObject>();
    }

    public virtual void Construct(Weapon weapon)
    {
        
    }

    public virtual bool UseLightAttack(EntityStamina stamina)
    {
        return stamina.Consume(_staminaConsume);
    }

    public virtual bool UseHeavyAttack(EntityStamina stamina)
    {
        return stamina.Consume(_heavyStaminaConsume);
    }

    private void OnEnable()
    {
        _entityAttack.OnStartLightAttack += HandleStartLightAttack;
        _entityAttack.OnStartHeavyAttack += HandleStartHeavyAttack;
        _entityAttack.OnEndAttack += HandleEndAttack;
    }

    private void OnDisable()
    {
        _entityAttack.OnStartLightAttack -= HandleStartLightAttack;
        _entityAttack.OnStartHeavyAttack -= HandleStartHeavyAttack;
        _entityAttack.OnEndAttack -= HandleEndAttack;
    }

    protected virtual bool HandleStartLightAttack()
    {
        _isHeavyAttack = false;
        EnableColliders(true);
        _objectsDamaged.Clear();
        return true;
    }

    protected virtual bool HandleStartHeavyAttack()
    {
        _isHeavyAttack = true;
        EnableColliders(true);
        _objectsDamaged.Clear();
        return true;
    }

    protected virtual void HandleEndAttack()
    {
        EnableColliders(false);
    }

    private void OnTriggerEnter(Collider collider)
    {
        GameObject target = collider.gameObject;
        
        // Should not deal damage to itself
        if (target == _stats.gameObject) return;

        // Should only deal damage one time per target
        if (_objectsDamaged.Contains(target)) return;
        _objectsDamaged.Add(target);

        EntityHealth health = target.GetComponent<EntityHealth>();
        if (health)
        {
            float damage = 0f;
            EntityStatsHolder targetStats = target.GetComponent<EntityStatsHolder>();
            
            if (_isHeavyAttack)
            {
                damage = DamageCalculator.HeavyMeleeAttack(HeavyDamage, _stats, targetStats);
                CameraShake.Instance.Shake(CameraShakePreset.HeavyAttack);
            }
            else
            {
                damage = DamageCalculator.LightMeleeAttack(LightDamage, _stats, targetStats);
                CameraShake.Instance.Shake(CameraShakePreset.Attack);
            }
            
            health.DealDamage(damage, _entity);
        }
    }

    private void EnableColliders(bool enable)
    {
        foreach (var item in _colliders)
        {
            item.enabled = enable;
        }
    }

}
