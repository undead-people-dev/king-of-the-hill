﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordWeapon : WeaponDamageDealer
{
    
    protected bool _isAttacking;
    protected float _rotationTime;
    protected float _currentRotationTime;
    protected float _angle;

    protected Transform _mesh;
    
    public override void Construct(Weapon weapon)
    {
        base.Construct(weapon);
        
        AnimatorOverrideController originalController = (AnimatorOverrideController) weapon.Controller;
        _rotationTime = originalController["Female Sword Attack 3"].length / 2f;
        _mesh = _entity.transform.GetChild(0);
    }

    private void Update()
    {
        if (_isAttacking && _isHeavyAttack && _currentRotationTime > 0)
        {
            _currentRotationTime -= Time.deltaTime;
            float t = _currentRotationTime / _rotationTime;
            _angle = Mathf.Lerp(360f, 0f, t);
            _mesh.localEulerAngles = new Vector3(0f, _angle, 0f);
        }
    }

    protected override bool HandleStartHeavyAttack()
    {
        base.HandleStartHeavyAttack();

        _currentRotationTime = _rotationTime;
        _angle = 0f;

        _isAttacking = true;
        return true;
    }

    protected override void HandleEndAttack()
    {
        base.HandleEndAttack();

        _isAttacking = false;
        _mesh.localRotation = Quaternion.identity;
    }

}
