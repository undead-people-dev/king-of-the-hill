﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Weapons/New Weapon")]
public class Weapon : ScriptableObject
{

    public enum WeaponType
    {
        Melee,
        Range
    }
    
    [Header("Info")]
    [SerializeField] private string _name = default;
    public string Name => _name;

    [SerializeField] [TextArea] private string _description = default;
    public string Description => _description;

    [SerializeField] private Sprite _icon = default;
    public Sprite Icon => _icon;

    [SerializeField] private WeaponType _type = default;
    public WeaponType Type => _type;

    [Header("Weapon")]
    [SerializeField] private GameObject _prefab = default;
    public GameObject Prefab => _prefab;
    
    [SerializeField] private RuntimeAnimatorController _controller = default;
    public RuntimeAnimatorController Controller => _controller;

    public enum Hand
    {
        Right,
        Left
    }
    [Tooltip("Select in which hand the weapon should be hold")]
    [SerializeField] private Hand _handHold = Hand.Right;
    public Hand HandHold => _handHold;

    public void Equip()
    {
        if (_type == WeaponType.Melee) Inventory.Main.EquipMeleeWeapon(this);
        else if (_type == WeaponType.Range) Inventory.Main.EquipRangeWeapon(this);
    }

}
