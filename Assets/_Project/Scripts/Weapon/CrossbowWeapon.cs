﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossbowWeapon : MonoBehaviour, IWeaponBehaviour
{
    

    [Header("Projectiles")]
    [SerializeField] protected List<GameObject> _projectiles = default;
    [Header("Animation")]
    [SerializeField] private AnimationClip _reloadClip = default;
    private AnimationClip _fireClip = default;
    
    protected Weapon _weapon = default;

    protected GameObject _entity;
    protected EntityAttack _entityAttack;

    protected int _projectileIndex;
    protected GameObject _currentProjectile => _projectiles[_projectileIndex];

    protected Animator _animator;
    protected AnimatorOverrideController _animatorOverrideController;
    protected bool _needReload;

    private void Awake()
    {
        _entityAttack = GetComponentInParent<EntityAttack>();
        _entity = _entityAttack.gameObject;

        _projectileIndex = 0;
        _animator = _entity.GetComponentInChildren<Animator>();
    }

    public void Construct(Weapon weapon)
    {
        _weapon = weapon;

        AnimatorOverrideController originalController = (AnimatorOverrideController) _weapon.Controller;
        _animatorOverrideController = new AnimatorOverrideController(originalController);
        _fireClip = originalController["Female Sword Attack 1"];
        _animatorOverrideController["Female Sword Attack 3"] = _fireClip;
    }

    public bool UseLightAttack(EntityStamina stamina)
    {
        return true;
    }

    public bool UseHeavyAttack(EntityStamina stamina)
    {
        return true;
    }

    private void OnEnable()
    {
        _animator.runtimeAnimatorController = _animatorOverrideController;

        _entityAttack.OnStartLightAttack += HandleStartLightAttack;
        _entityAttack.OnStartHeavyAttack += HandleStartHeavyAttack;
        _entityAttack.OnEndAttackAnimation += HandleEndAttack;
    }

    private void OnDisable()
    {
        _entityAttack.OnStartLightAttack -= HandleStartLightAttack;
        _entityAttack.OnStartHeavyAttack -= HandleStartHeavyAttack;
        _entityAttack.OnEndAttackAnimation -= HandleEndAttack;
    }

    private bool HandleStartLightAttack()
    {
        if (!_needReload)
        {
            _animatorOverrideController["Female Sword Attack 1"] = _fireClip;

            Vector3 spawnPoint = _entity.transform.position + Vector3.up + _entity.transform.forward;
            
            GameObject created = Instantiate(_currentProjectile, spawnPoint, Quaternion.LookRotation(_entityAttack.LookDirection));

            Projectile projectile = created.GetComponent<Projectile>();
            projectile.Fire(_entity, _entityAttack.LookDirection);

            _needReload = true;
        }
        else
        {
            _animatorOverrideController["Female Sword Attack 1"] = _reloadClip;
            _needReload = false;
        }
        return true;
    }

    private bool HandleStartHeavyAttack()
    {
        _projectileIndex = (_projectileIndex + 1) % _projectiles.Count;
        return true;
    }

    private void HandleEndAttack()
    {

    }

}
