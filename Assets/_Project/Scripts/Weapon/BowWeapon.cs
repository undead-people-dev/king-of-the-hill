﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowWeapon : MonoBehaviour, IWeaponBehaviour
{

    [Header("Projectiles")]
    [SerializeField] private GameObject _projectilePrefab = default;
    [SerializeField] protected int _ammo = 25;
    [Header("Heavy")]
    [SerializeField] private int _heavyArrowUse = 10;
    [SerializeField] private float _heavyAngle = 25f;
    [SerializeField] private float _heavyStaminaConsume = 35f;


    protected GameObject _entity;
    protected EntityAttack _entityAttack;

    protected bool _isHeavyAttack;

    private void Awake()
    {
        _entityAttack = GetComponentInParent<EntityAttack>();
        _entity = _entityAttack.gameObject;
    }

    public void Construct(Weapon weapon)
    {

    }

    public bool UseLightAttack(EntityStamina stamina)
    {
        return _ammo > 0;
    }

    public bool UseHeavyAttack(EntityStamina stamina)
    {
        return _ammo > _heavyArrowUse && stamina.Consume(_heavyStaminaConsume);
    }

    private void OnEnable()
    {
        _entityAttack.OnStartLightAttack += HandleStartLightAttack;
        _entityAttack.OnStartHeavyAttack += HandleStartHeavyAttack;
        _entityAttack.OnEndAttackAnimation += HandleEndAttack;
    }

    private void OnDisable()
    {
        _entityAttack.OnStartLightAttack -= HandleStartLightAttack;
        _entityAttack.OnStartHeavyAttack -= HandleStartHeavyAttack;
        _entityAttack.OnEndAttackAnimation -= HandleEndAttack;
    }

    private bool HandleStartLightAttack()
    {
        _isHeavyAttack = false;
        return true;
    }

    private bool HandleStartHeavyAttack()
    {
        _isHeavyAttack = true;
        return true;
    }

    private void HandleEndAttack()
    {
        Vector3 spawnPoint = _entity.transform.position + Vector3.up + _entity.transform.forward;
        
        if (!_isHeavyAttack)
        {
            _ammo -= 1;
            GameObject created = Instantiate(_projectilePrefab, spawnPoint, Quaternion.LookRotation(_entityAttack.LookDirection));
            Projectile projectile = created.GetComponent<Projectile>();
            projectile.Fire(_entity, _entityAttack.LookDirection);
        }
        else
        {
            _ammo -= _heavyArrowUse;
            
            float angleOffset = -(_heavyAngle / 2f);
            float eachAngle = _heavyAngle / (_heavyArrowUse - 1);
            
            for (int i = 0; i < _heavyArrowUse; i++)
            {
                float degAngle = (angleOffset + (i * eachAngle));
                Vector3 worldDirection = Quaternion.AngleAxis(degAngle, Vector3.up) * _entityAttack.LookDirection;
                
                GameObject created = Instantiate(_projectilePrefab, spawnPoint, Quaternion.LookRotation(worldDirection));
                Projectile projectile = created.GetComponent<Projectile>();
                projectile.Fire(_entity, worldDirection);
            }
        }

    }

}
