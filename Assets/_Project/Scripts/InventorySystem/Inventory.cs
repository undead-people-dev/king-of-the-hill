﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory
{
    
    public delegate void ChangeEquipmentHandler();
    public event ChangeEquipmentHandler OnChangeEquipment;

    private static Inventory s_main;
    public static Inventory Main { get => s_main; }

    private Stats _baseStats;
    public Stats BaseStats => _baseStats;

    private List<Weapon> _weapons;
    public List<Weapon> Weapons => _weapons;
    private List<Equipment> _equipments;
    public List<Equipment> Equipments => _equipments;

    private int _gold;
    public int Gold => _gold;
    private int _fame;
    public int Fame => _fame;

    private Weapon _meleeWeapon;
    public Weapon MeleeWeapon => _meleeWeapon;

    private Weapon _rangeWeapon;
    public Weapon RangeWeapon => _rangeWeapon;

    private HeadEquipment _head;
    public HeadEquipment Head => _head;

    private BodyEquipment _body;
    public BodyEquipment Body => _body;
    
    private RingEquipment _ring;
    public RingEquipment Ring => _ring;

    public Inventory()
    {
        s_main = this;

        _baseStats = new Stats();
        _baseStats.Gladiator = 10;
        _baseStats.Marksman = 10;
        _baseStats.Athlete = 10;
        _baseStats.Acrobat = 10;
        _baseStats.Warrior = 10;
        _baseStats.Performer = 10;

        _weapons = new List<Weapon>();
        _equipments = new List<Equipment>();

        _gold = 0;
        _fame = 0;
    }

    public bool EquipHead(Equipment equipment)
    {
        if (equipment is HeadEquipment is false)
        {
            return false;
        }

        _head = (HeadEquipment) equipment;
        OnChangeEquipment?.Invoke();
        return true;
    }

    public bool EquipBody(Equipment equipment)
    {
        if (equipment is BodyEquipment is false)
        {
            return false;
        }

        _body = (BodyEquipment) equipment;
        OnChangeEquipment?.Invoke();
        return true;
    }

    public bool EquipRing(Equipment equipment)
    {
        if (equipment is RingEquipment is false)
        {
            return false;
        }

        _ring = (RingEquipment) equipment;
        OnChangeEquipment?.Invoke();
        return true;
    }

    public bool EquipMeleeWeapon(Weapon weapon)
    {
        if (weapon.Type != Weapon.WeaponType.Melee)
        {
            return false;
        }

        _meleeWeapon = weapon;
        OnChangeEquipment?.Invoke();
        return true;
    }

    public bool EquipRangeWeapon(Weapon weapon)
    {
        if (weapon.Type != Weapon.WeaponType.Range)
        {
            return false;
        }

        _rangeWeapon = weapon;
        OnChangeEquipment?.Invoke();
        return true;
    }

    public void AddEquipment(Equipment equipment)
    {
        _equipments.Add(equipment);
    }

    public void AddWeapon(Weapon weapon)
    {
        _weapons.Add(weapon);
    }

    public void AddGold(int amount)
    {
        _gold += amount;
    }

    public void AddFame(int amount)
    {
        _fame += amount;
    }

}
