﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsHolder : EntityStatsHolder
{

    protected override Stats ComputeStats()
    {
        _baseStats = new Stats(Inventory.Main.BaseStats);
        _head = Inventory.Main.Head;
        _body = Inventory.Main.Body;
        _ring = Inventory.Main.Ring;

        return base.ComputeStats();
    }
}
