﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerLook : MonoBehaviour
{

	public Vector3 LookDirection
	{
		get
		{
			Ray ray = _mainCamera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
			return ray.direction;
		}
	} 

	[SerializeField] private float _sensitivity = 3F;
	[SerializeField] private float _minimumX = -360F;
	[SerializeField] private float _maximumX = 360F;
	[SerializeField] private float _minimumY = -60F;
	[SerializeField] private float _maximumY = 60F;

	private float _rotationX = 0F;
	private float _rotationY = 0F;
	private Quaternion _originalRotation;

	private Camera _mainCamera;
    
    private CinemachineVirtualCamera _cinemachine;
    private CinemachineBrain _cinemachineBrain;
    private CinemachineTransposer _cinemachineTransposer;
	private Vector3 _originalOffset;

    private void Awake()
    {
		_mainCamera = Camera.main;

        _cinemachine = _mainCamera.transform.parent.GetComponentInChildren<CinemachineVirtualCamera>();
        _cinemachineBrain = _mainCamera.transform.parent.GetComponentInChildren<CinemachineBrain>();
        _cinemachineTransposer = _cinemachine.GetCinemachineComponent<CinemachineTransposer>();
    }

	private void Start()
	{
		_originalRotation = transform.rotation;

		_originalOffset = _cinemachineTransposer.m_FollowOffset;
	}

	private void OnEnable()
	{
		Cursor.lockState = CursorLockMode.Locked;
	}

	private void OnDisable()
	{
		Cursor.lockState = CursorLockMode.None;
	}

    private void Update()
    {
        
		// Read the mouse input axis
		_rotationX += Input.GetAxis ("Mouse X") * _sensitivity;
		_rotationY += Input.GetAxis ("Mouse Y") * _sensitivity;
		Vector3 offset = _originalOffset + (Vector3.down * _rotationY * .05f); 
		if (offset.y < 0f) offset.y = 0f;
		if (offset.y > 8f) offset.y = 8f;
		_cinemachineTransposer.m_FollowOffset = offset;

		_rotationX = ClampAngle (_rotationX, _minimumX, _maximumX);
		_rotationY = ClampAngle (_rotationY, _minimumY, _maximumY);

		Quaternion xQuaternion = Quaternion.AngleAxis (_rotationX, Vector3.up);
		Quaternion yQuaternion = Quaternion.AngleAxis (_rotationY, -Vector3.right);

		transform.localRotation = _originalRotation * xQuaternion;
    }

	private float ClampAngle (float angle, float min, float max)
    {
		if (angle < -360f) angle += 360f;
		if (angle > 360f) angle -= 360f;
		return Mathf.Clamp (angle, min, max);
	}

}
