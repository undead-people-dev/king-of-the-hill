﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour
{

    private StateMachine _stateMachine;

    private void Awake()
    {

        EntityAttack atack = GetComponent<EntityAttack>();

        _stateMachine = GetComponent<StateMachine>();
        _stateMachine.Construct(new Dictionary<StateType, BaseState>() {
            { StateType.Initial, new EntityWaitingState(gameObject) },
            { StateType.Move, new PlayerMoveState(gameObject) },
            { StateType.Roll, new PlayerRollingState(gameObject) },
            { StateType.LightAttack, new PlayerLightAttackState(atack, gameObject) },
            { StateType.HeavyAttack, new PlayerHeavyAttackState(atack, gameObject) },
            { StateType.Death, new EntityDeathState(gameObject) },
        });
        
        EntityHealth health = GetComponent<EntityHealth>();
        health.OnDeath += () => _stateMachine.ChangeState(StateType.Death);
    }

}
