using System;
using UnityEngine;

public class PlayerLightAttackState : BaseState
{    

    private EntityAttack _attack;

    public PlayerLightAttackState(EntityAttack attack, GameObject gameObject) : base(gameObject)
    {
        _attack = attack;
    }

    public override void Start()
    {

        if (!_attack.StartLightAttack())
        {
            _stateMachine.ChangeState(StateType.Move);
            return;
        }
        
        _attack.OnEndAttack += HandleEndAttack;
    }

    public override void Stop()
    {
        _attack.OnEndAttack -= HandleEndAttack;
    }

    public override void Tick()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            _attack.StopLightAttack();
        }
    }

    private void HandleEndAttack()
    {
        _stateMachine.ChangeState(StateType.Move);
    }

}