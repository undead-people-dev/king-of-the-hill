using UnityEngine;

public class PlayerHeavyAttackState : BaseState
{    

    private EntityAttack _attack;

    public PlayerHeavyAttackState(EntityAttack attack, GameObject gameObject) : base(gameObject)
    {
        _attack = attack;
    }

    public override void Start()
    {
        if (!_attack.StartHeavyAttack())
        {
            _stateMachine.ChangeState(StateType.Move);
            return;
        }
        
        _attack.OnEndAttack += HandleEndAttack;
    }

    public override void Stop()
    {
        _attack.OnEndAttack -= HandleEndAttack;
    }

    public override void Tick()
    {
        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            _attack.StopHeavyAttack();
        }
    }

    private void HandleEndAttack()
    {
        _stateMachine.ChangeState(StateType.Move);
    }
    
}