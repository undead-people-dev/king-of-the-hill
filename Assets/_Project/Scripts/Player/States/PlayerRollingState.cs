using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRollingState : BaseState
{    

    private EntityHealth _health;
    private PlayerMovement _movement;
    private EntityAnimationEventsListener _animationListener;

    private Vector3 _rollDirection;

    public PlayerRollingState(GameObject gameObject) : base(gameObject)
    {
        _movement = gameObject.GetComponent<PlayerMovement>();
        _health = gameObject.GetComponent<EntityHealth>();
        _animationListener = gameObject.GetComponentInChildren<EntityAnimationEventsListener>();
    }

    public override void Start()
    {
        _rollDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        if (_rollDirection == Vector3.zero) _rollDirection = Vector3.forward;
        _rollDirection.Normalize();
        
        if (!_movement.Roll(_rollDirection))
        {
            _stateMachine.ChangeState(StateType.Move);
            return;
        }
        
        _health.Invulnerable();

        _animationListener.OnEndRoll += HandleEndRoll;
    }

    public override void Stop()
    {
        _health.Vunerable();
        _animationListener.OnEndRoll -= HandleEndRoll;
    }

    public override void Tick()
    {
    }

    private void HandleEndRoll()
    {
        _stateMachine.ChangeState(StateType.Move);
    }

}