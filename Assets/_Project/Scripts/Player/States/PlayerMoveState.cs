using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveState : BaseState
{    

    private PlayerMovement _movement;
    private EntityAttack _attack;

    public PlayerMoveState(GameObject gameObject) : base(gameObject)
    {
        _movement = gameObject.GetComponent<PlayerMovement>();
        _attack = gameObject.GetComponent<EntityAttack>();
    }

    public override void Start()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            _movement.StartRunning();
        }
    }

    public override void Stop()
    {
        _movement.StopRunning();
    }

    public override void Tick()
    {
        _movement.Motion = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            _stateMachine.ChangeState(StateType.LightAttack);
            return;
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            _stateMachine.ChangeState(StateType.HeavyAttack);
            return;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _stateMachine.ChangeState(StateType.Roll);
            return;
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            _attack.NextWeapon();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            _movement.StartRunning();
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            _movement.StopRunning();
        }
    }

}