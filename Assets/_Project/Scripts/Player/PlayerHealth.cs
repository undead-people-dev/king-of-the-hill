﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : EntityHealth
{

    protected override void Death()
    {
        
    }

    protected override void TakeDamage(float damage, GameObject attacker)
    {
        CameraShake.Instance.Shake(CameraShakePreset.Damage);
    }
    
}
