﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(EntityStamina))]
public class PlayerMovement : MonoBehaviour
{
    
    [SerializeField] protected float _drag = 50f;
    [SerializeField] protected float _acceleration = 50f;
    [SerializeField] protected float _rollSpeed = 12f;
    [Header("Base values")]
    [SerializeField] protected float _baseMoveSpeed = 3f;
    [SerializeField] protected float _baseRunSpeed = 7f;

    protected float _moveSpeed = 3f;
    protected float _runSpeed = 7f;

    protected Vector3 _motion;
    public Vector3 Motion { get => _motion; set => _motion = value.normalized; }

    protected CharacterController _characterController;
    protected Animator _animator;
    protected EntityStamina _entityStamina;

    protected Vector3 _targetMoveVelocity;
    protected Vector3 _moveVelocity;
    public Vector3 MoveVelocity { get => _moveVelocity; }

    protected Vector3 _extraVelocity;
    public Vector3 ExtraVelocity { get => _extraVelocity; set => _extraVelocity = value; }

    private bool _isRunning;
    public bool IsRunning { get => _isRunning; set => _isRunning = value; }

    protected virtual void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _animator = GetComponentInChildren<Animator>();
        _entityStamina = GetComponent<EntityStamina>();
    }

    protected virtual void Start()
    {
        EntityStatsHolder stats = GetComponent<EntityStatsHolder>();

        float multiplierSpeed = stats.MovementSpeedMultiplier();
        _moveSpeed = multiplierSpeed * _baseMoveSpeed;
        _runSpeed = multiplierSpeed * _baseRunSpeed;
        _animator.SetFloat("MoveSpeedMultipler", multiplierSpeed);
    }

    public void StartRunning()
    {
        _isRunning = true;
    }
    
    public void StopRunning()
    {
        _isRunning = false;
    }

    public bool Roll(Vector3 direction)
    {
        if (!_entityStamina.Consume(10f)) return false;

        _extraVelocity = direction * _rollSpeed;
        _animator.SetTrigger("Roll");
        return true;
    }
    
    protected virtual float ComputeSpeed()
    {
        if (_isRunning)
        {
            return _runSpeed;
        }
        else
        {
            return _moveSpeed;
        }
    }

    protected virtual float ComputeStaminaConsume()
    {
        if (_isRunning)
        {
            return 1f;
        }
        else
        {
            return 0f;
        }
    }

    protected virtual void Update()
    {

        float currentSpeed = ComputeSpeed();
        float staminaConsume = ComputeStaminaConsume();

        if (!_entityStamina.Consume(staminaConsume * Time.deltaTime))
        {
            StopRunning();
        }

        _targetMoveVelocity = _motion * currentSpeed;
        _moveVelocity = Vector3.MoveTowards(_moveVelocity, _targetMoveVelocity, _acceleration * Time.deltaTime);
        _extraVelocity = Vector3.MoveTowards(_extraVelocity, Vector3.zero, _drag * Time.deltaTime);

        Vector3 velocity = _moveVelocity + _extraVelocity;
        Vector3 worldVelocity = transform.TransformVector(velocity);
	    CollisionFlags collisionResult = _characterController.Move(worldVelocity * Time.deltaTime);
		if(collisionResult == CollisionFlags.Above && _moveVelocity.y > 0f)
		{
			_moveVelocity.y = 0f;
		}

        _motion = Vector3.zero;

    }

    private void LateUpdate()
    {
        Vector3 speed = _moveVelocity / _runSpeed;
        _animator.SetFloat("SpeedX", speed.x);
        _animator.SetFloat("SpeedZ", speed.z);
    }
}
