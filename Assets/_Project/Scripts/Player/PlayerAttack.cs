using UnityEngine;

public class PlayerAttack : EntityAttack
{

    public override Vector3 LookDirection => _look.LookDirection;

    private PlayerLook _look;
    
    protected override void Awake()
    {
        _weapons.Clear();
        
        _weapons.Add(Inventory.Main.MeleeWeapon);
        _weapons.Add(Inventory.Main.RangeWeapon);

        base.Awake();
        
        _look = GetComponent<PlayerLook>();
    }

}