﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Equipment : ScriptableObject
{

    [Header("Info")]
    [SerializeField] private string _name = default;
    public string Name => _name;

    [SerializeField] [TextArea] private string _description = default;
    public string Description => _description;

    [SerializeField] private Sprite _icon = default;
    public Sprite Icon => _icon;

    [Header("Stats")]
    [SerializeField] private Stats _stats = default;
    public Stats Stats => _stats;

    public abstract void Equip();

}
