using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Equipments/New Body Equipment")]
public class BodyEquipment : Equipment
{

    public override void Equip()
    {
        Inventory.Main.EquipBody(this);
    }

}
