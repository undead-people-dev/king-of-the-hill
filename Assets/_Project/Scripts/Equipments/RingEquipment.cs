using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Equipments/New Ring Equipment")]
public class RingEquipment : Equipment
{

    public override void Equip()
    {
        Inventory.Main.EquipRing(this);
    }
    
}
