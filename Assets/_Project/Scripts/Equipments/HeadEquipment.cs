using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Equipments/New Head Equipment")]
public class HeadEquipment : Equipment
{

    public override void Equip()
    {
        Inventory.Main.EquipHead(this);
    }
    
}
