﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUDEntity : MonoBehaviour
{

    [Header("Health")]
    [SerializeField] private Image _healthImage = default;
    [SerializeField] private TextMeshProUGUI _healthText = default;

    [Header("Stamina")]
    [SerializeField] private Image _staminaImage = default;
    [SerializeField] private TextMeshProUGUI _staminaText = default;

    private EntityHealth _health;
    private EntityStamina _stamina;

    private Color _originalStaminaColor;

    private void Awake()
    {
        _health = GetComponentInParent<EntityHealth>();
        _stamina = GetComponentInParent<EntityStamina>();
        if (_health == null && _stamina == null)
        {
            GameObject player = GameObject.FindObjectOfType<PlayerHealth>().gameObject;
            _health = player.GetComponent<EntityHealth>();
            _stamina = player.GetComponent<EntityStamina>();
        }
        _originalStaminaColor = _staminaImage.color;
    }

    private void Start()
    {
        UpdateHealthText();
        UpdateStaminaText();
    }

    private void OnEnable()
    {
        _health.OnTakeDamage += HandleTakeDamage;
        _health.OnDeath += HandleDeath;
        _stamina.OnConsume += HandleStaminaConsume;
        _stamina.OnConsumeFail += HandleStaminaConsumeFail;
    }

    private void OnDisable()
    {
        _health.OnTakeDamage -= HandleTakeDamage;
        _health.OnDeath -= HandleDeath;
        _stamina.OnConsume -= HandleStaminaConsume;
        _stamina.OnConsumeFail -= HandleStaminaConsumeFail;
    }

    private void HandleTakeDamage()
    {
        UpdateHealthText();
    }

    private void HandleStaminaConsume()
    {
        UpdateStaminaText();
    }

    private void UpdateHealthText()
    {
        _healthImage.fillAmount = Mathf.Clamp01(_health.Health / _health.MaxHealth);
        _healthText.text = string.Format("{0} / {1}", Mathf.RoundToInt(_health.MaxHealth), Mathf.RoundToInt(_health.Health));
    }

    private void UpdateStaminaText()
    {
        _staminaImage.fillAmount = Mathf.Clamp01(_stamina.Stamina / _stamina.MaxStamina);
        _staminaText.text = string.Format("{0} / {1}", Mathf.RoundToInt(_stamina.MaxStamina), Mathf.RoundToInt(_stamina.Stamina));
    }

    private void HandleStaminaConsumeFail()
    {
        StartCoroutine(ConsumeFailCoroutine());
    }

    private IEnumerator ConsumeFailCoroutine()
    {
        for (int i = 0; i < 10; i++)
        {
            _staminaImage.color = i % 2 == 1 ? _originalStaminaColor : Color.white;
            yield return new WaitForSeconds(.1f);
        }

        _staminaImage.color = _originalStaminaColor;
    }

    private void HandleDeath()
    {
        gameObject.SetActive(false);
    }

}
