﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stats
{
    
    [SerializeField] private int _gladiator = 0;
    public int Gladiator { get => _gladiator; set => _gladiator = value; }

    [SerializeField] private int _marksman = 0;
    public int Marksman { get => _marksman; set => _marksman = value; }

    [SerializeField] private int _athlete = 0;
    public int Athlete { get => _athlete; set => _athlete = value; }

    [SerializeField] private int _acrobat = 0;
    public int Acrobat { get => _acrobat; set => _acrobat = value; }

    [SerializeField] private int _warrior = 0;
    public int Warrior { get => _warrior; set => _warrior = value; }

    [SerializeField] private int _performer = 0;
    public int Performer { get => _performer; set => _performer = value; }

    public Stats(Stats stats)
    {
        _gladiator = stats._gladiator;
        _marksman = stats._marksman;
        _athlete = stats._athlete;
        _acrobat = stats._acrobat;
        _warrior = stats._warrior;
        _performer = stats._performer;
    }

    public Stats(int gladiator = 0, int marksman = 0, int athlete = 0, int acrobat = 0, int warrior = 0, int performer = 0)
    {
        _gladiator = gladiator;
        _marksman = marksman;
        _athlete = athlete;
        _acrobat = acrobat;
        _warrior = warrior;
        _performer = performer;
    }

    public static Stats Empty => new Stats();

    public static Stats operator +(Stats f1, Stats f2) 
    {
        return new Stats(
            f1._gladiator + f2._gladiator,
            f1._marksman + f2._marksman,
            f1._athlete + f2._athlete,
            f1._acrobat + f2._acrobat,
            f1._warrior + f2._warrior,
            f1._performer + f2._performer
        );
    }

    public static Stats operator -(Stats f1, Stats f2) 
    {
        return new Stats(
            f1._gladiator - f2._gladiator,
            f1._marksman - f2._marksman,
            f1._athlete - f2._athlete,
            f1._acrobat - f2._acrobat,
            f1._warrior - f2._warrior,
            f1._performer - f2._performer
        );
    }

}