﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveProjectile : Projectile
{
    
    [SerializeField] private GameObject _explosionPrefab = default;

    protected override void HitTarget(GameObject target)
    {
        base.HitTarget(target);
        Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
    }

}
