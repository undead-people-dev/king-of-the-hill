﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    
    [SerializeField] private float _minDamage = 10f;
    public float MinDamage => _minDamage;

    [SerializeField] private float _maxDamage = 10f;
    public float MaxDamage => _maxDamage;

    public float Damage => Random.Range(_minDamage, _maxDamage);

    [SerializeField] private float _speed = 10f;

    private Rigidbody _rigidbody;
    private GameObject _attacker;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void Fire(GameObject attacker, Vector3 direction)
    {
        _attacker = attacker;
        _rigidbody.velocity = direction * _speed;
    }

    private void OnTriggerEnter(Collider collider)
    {
        HitTarget(collider.gameObject);
    }

    protected virtual void HitTarget(GameObject target)
    {    
        IHealth health = target.GetComponent<IHealth>();
        if (health != null)
        {
            float damage = DamageCalculator.RangeAttack(this, _attacker, target);
            health.DealDamage(damage, _attacker);
        }
        
        Destroy(gameObject);
    }
}
