﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityRangeAttack : MonoBehaviour, IAttack
{

    public virtual bool StartLightAttack()
    {
        Debug.Log("StartLightAttack");
        return true;
    }

    public virtual void StopLightAttack()
    {
        Debug.Log("StopLightAttack");
    }

    public virtual bool StartHeavyAttack()
    {
        Debug.Log("StartHeavyAttack");
        return true;
    }

    public virtual void StopHeavyAttack()
    {
        Debug.Log("StopHeavyAttack");
    }

}
