﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EntityStatsHolder : MonoBehaviour
{
    
    [SerializeField] protected Stats _baseStats = default;
    public Stats BaseStats => _baseStats;
        
    [Header("Equipments")]
    [SerializeField] protected HeadEquipment _head = default;
    public HeadEquipment Head => _head;
    [SerializeField] protected BodyEquipment _body = default;
    public BodyEquipment Body => _body;
    [SerializeField] protected RingEquipment _ring = default;
    public RingEquipment Ring => _ring;
        
    protected Stats _stats = null;
    public Stats Stats
    {
        get
        {
            if (_stats == null) _stats = ComputeStats();
            return _stats;
        }
    }

    protected virtual Stats ComputeStats()
    {
        Stats stats = _baseStats; 

        if (_head != null) stats += _head.Stats;
        if (_body != null) stats += _body.Stats;
        if (_ring != null) stats += _ring.Stats;
        
        return stats;
    }

    /// <summary>
    /// Get the max health value based on Warrior stat
    /// </summary>
    public float MaxHealth()
    {
        return Stats.Warrior * 15f;
    }
    
    /// <summary>
    /// Get the max stamina value based on Athlete stat
    /// </summary>
    public float MaxStamina()
    {
        return Stats.Athlete * 10f;
    }

    /// <summary>
    /// Get the Movement Speed Multiplier based on Acrobat stat
    /// </summary>
    public float MovementSpeedMultiplier()
    {
        return 1f + (NormalizeStat(Stats.Acrobat) / 100f);
    }

    /// <summary>
    /// Get the Action Speed Multiplier based on Acrobat stat
    /// </summary>
    public float ActionSpeedMultiplier()
    {
        return 1f + (NormalizeStat(Stats.Acrobat) / 150f);
    }

    /// <summary>
    /// Get the Damage value from a Light Attack based on Gladiator stat
    /// </summary>
    public float LightAttackDamage()
    {
        return NormalizeStat(Stats.Gladiator) * 1f;
    }

    /// <summary>
    /// Get the Damage value from a Heavy Attack based on Gladiator stat
    /// </summary>
    public float HeavyAttackDamage()
    {
        return NormalizeStat(Stats.Gladiator) * 1.7f;
    }

    /// <summary>
    /// Get the Range Damage value based on Marksman stat
    /// </summary>
    public float RangeAttackDamage()
    {
        return NormalizeStat(Stats.Marksman) * 1f;
    }

    /// <summary>
    /// Get the Gold Reward Multiplier based on Performer stat
    /// </summary>
    /// <returns>Positive multiplier value</returns>
    public float GoldRewardMultiplier()
    {
        return Mathf.Abs(NormalizeStat(Stats.Performer) * 5f);
    }

    /// <summary>
    /// Get the Fame Reward Multiplier based on Performer stat
    /// </summary>
    /// <returns>Positive multiplier value</returns>
    public float FameRewardMultiplier()
    {
        return Mathf.Abs(NormalizeStat(Stats.Performer) * 5f);
    }

    private int NormalizeStat(int stat)
    {
        return stat - 10;
    }
}
