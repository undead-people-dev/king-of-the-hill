﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EntityStamina))]
[RequireComponent(typeof(EntityStatsHolder))]
public class EntityAttack : MonoBehaviour, IAttack
{
    
    public delegate bool StartLightAttackHandler();
    public delegate bool StartHeavyAttackHandler();
    public delegate void EndAttackAnimationHandler();
    public delegate void EndAttackHandler();
    
    public event StartLightAttackHandler OnStartLightAttack;
    public event StartHeavyAttackHandler OnStartHeavyAttack;
    public event EndAttackAnimationHandler OnEndAttackAnimation;
    public event EndAttackHandler OnEndAttack;

    public virtual Vector3 LookDirection => transform.forward;

    [SerializeField] protected Transform _rightHandTransform = default;
    [SerializeField] protected Transform _leftHandTransform = default;

    [SerializeField] protected List<Weapon> _weapons = default;
    public List<Weapon> Weapons => _weapons;

    protected int _weaponsIndex;
    protected List<GameObject> _weaponsGameObjects;
    protected List<IWeaponBehaviour> _weaponsBehaviour;

    protected Weapon _currentWeapon => _weapons[_weaponsIndex];
    protected GameObject _currentWeaponObject => _weaponsGameObjects[_weaponsIndex];
    protected IWeaponBehaviour _currentWeaponBehaviour => _weaponsBehaviour[_weaponsIndex];

    protected Animator _animator;
    protected EntityAnimationEventsListener _animationListener;
    protected EntityStamina _entityStamina;
    protected EntityStatsHolder _statsHolder;

    protected bool _performingHeavyAttack;
    protected bool _stoppedAttacking;

    protected virtual void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
        _animationListener = GetComponentInChildren<EntityAnimationEventsListener>();
        _entityStamina = GetComponent<EntityStamina>();
        _statsHolder = GetComponent<EntityStatsHolder>();

        _weaponsGameObjects = new List<GameObject>();
        _weaponsBehaviour = new List<IWeaponBehaviour>();
        foreach (var weapon in _weapons)
        {
            GameObject created = null;
            if (weapon.HandHold == Weapon.Hand.Right)
            {
                created = Instantiate(weapon.Prefab, _rightHandTransform);
            }
            else if (weapon.HandHold == Weapon.Hand.Left)
            {
                created = Instantiate(weapon.Prefab, _leftHandTransform);
            }
            created.SetActive(false);
            IWeaponBehaviour weaponBehaviour = created.GetComponentInChildren<IWeaponBehaviour>();
            weaponBehaviour?.Construct(weapon);
            
            _weaponsGameObjects.Add(created);
            _weaponsBehaviour.Add(weaponBehaviour);
        }

        _weaponsIndex = 0;
        ActiveCurrentWeapon();
    }

    private void Start()
    {
        _animator.SetFloat("AttackSpeedMultipler", _statsHolder.ActionSpeedMultiplier());
    }

    protected void OnEnable()
    {
        _animationListener.OnEndAttack += HandleEndAttack;
    }

    protected void OnDisable()
    {
        _animationListener.OnEndAttack -= HandleEndAttack;
    }

    public virtual bool StartLightAttack()
    {
        _performingHeavyAttack = false;
        
        return PerformAttack();
    }

    public virtual void StopLightAttack()
    {
        StopAttack();
    }

    public virtual bool StartHeavyAttack()
    {
        _performingHeavyAttack = true;

        return PerformAttack();
    }

    public virtual void StopHeavyAttack()
    {
        StopAttack();
    }

    public void NextWeapon()
    {
        _currentWeaponObject.SetActive(false);
        _weaponsIndex = (_weaponsIndex + 1) % _weapons.Count;
        ActiveCurrentWeapon();
    }

    private void ActiveCurrentWeapon()
    {
        _animator.runtimeAnimatorController = _currentWeapon.Controller;
        _currentWeaponObject.SetActive(true);
    }


    protected void StopAttack()
    {
        _stoppedAttacking = true;
    }

    protected void HandleEndAttack()
    {
        OnEndAttackAnimation?.Invoke();
        if (!_stoppedAttacking) 
        {
            PerformAttack();
        }
        else
        {
            FinishAttack();
        }
    }

    private bool PerformAttack()
    {
        if (!_performingHeavyAttack)
        {
            OnStartLightAttack?.Invoke();
            
            if (!_currentWeaponBehaviour.UseLightAttack(_entityStamina))
            {
                FinishAttack();
                return false;
            }

            _animator.SetTrigger("StartAttack");
        }
        else
        {            
            OnStartHeavyAttack?.Invoke();

            if (!_currentWeaponBehaviour.UseHeavyAttack(_entityStamina))
            {
                FinishAttack();
                return false;
            }

            _animator.SetTrigger("StartHeavyAttack");
        }

        _stoppedAttacking = false;
        return true;
    }

    protected void FinishAttack()
    {
        OnEndAttack?.Invoke();
    }

}
