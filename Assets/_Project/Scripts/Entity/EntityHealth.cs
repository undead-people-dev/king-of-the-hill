﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EntityStatsHolder))]
public class EntityHealth : MonoBehaviour, IHealth
{
    
    public delegate void TakeDamageHandler();
    public delegate void DeathHandler();

    public event TakeDamageHandler OnTakeDamage;
    public event DeathHandler OnDeath;

    [SerializeField] protected GameObject _floatingTextPrefab = default;
    [SerializeField] protected GameObject _hitPrefab = default;

    protected float _maxHealth = 100f;
    public float MaxHealth => _maxHealth;

    protected float _health;
    public float Health => _health;

    protected EntityStatsHolder _statsHolder;
    protected Animator _animator;

    protected bool _isInvulnerable;

    private void Awake()
    {
        _statsHolder = GetComponent<EntityStatsHolder>();
        _animator = GetComponentInChildren<Animator>();
        _maxHealth = _statsHolder.MaxHealth();
        _health = _maxHealth;
    }

    public void Invulnerable()
    {
        _isInvulnerable = true;
    }

    public void Vunerable()
    {
        _isInvulnerable = false;
    }

    public virtual void DealDamage(float damage, GameObject attacker)
    {
        if (_isInvulnerable) return;

        _health -= damage;

        Vector3 contactPoint = transform.position + Vector3.up;
        
        Instantiate(_hitPrefab, contactPoint, Quaternion.identity);
        FloatingText text = Instantiate(_floatingTextPrefab, contactPoint, Quaternion.identity).GetComponent<FloatingText>();
        text.Construct(Mathf.RoundToInt(damage).ToString(), Color.red, 2f, .5f);
        
        if (_health <= 0f)
        {
            OnDeath?.Invoke();
            Death();
        }
        else
        {
            OnTakeDamage?.Invoke();
            TakeDamage(damage, attacker);
        }
    }

    protected virtual void Death()
    {
        Destroy(GetComponent<CharacterController>());
        Destroy(GetComponent<UnityEngine.AI.NavMeshAgent>());
    }

    protected virtual void TakeDamage(float damage, GameObject attacker)
    {
        Debug.LogFormat("{0}|TAKE_DAMAGE|{1}", gameObject.name, _health);
    }

}
