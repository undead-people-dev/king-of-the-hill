﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EntityStatsHolder))]
public class EntityStamina : MonoBehaviour, IStamina
{
    
    public delegate void ConsumeHandler();
    public delegate void ConsumeFailHandler();
    
    public event ConsumeHandler OnConsume;
    public event ConsumeFailHandler OnConsumeFail;

    [SerializeField] private float _recover = 1f;

    private float _maxStamina;
    public float MaxStamina { get => _maxStamina; }

    private float _stamina;
    public float Stamina { get => _stamina; }

    protected EntityStatsHolder _statsHolder;

    private void Awake()
    {
        _statsHolder = GetComponent<EntityStatsHolder>();
        _maxStamina = _statsHolder.MaxStamina();
        _stamina = _maxStamina;
    }

    public bool Consume(float stamina)
    {
        if (_stamina < stamina)
        {
            OnConsumeFail?.Invoke();
            return false;
        }

        _stamina -= stamina;
        OnConsume?.Invoke();
        return true;
    }

    private void Update()
    {
        _stamina = Mathf.MoveTowards(_stamina, _maxStamina, _recover * Time.deltaTime);
    }

}
