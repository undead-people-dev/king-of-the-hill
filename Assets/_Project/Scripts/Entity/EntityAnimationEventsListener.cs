﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityAnimationEventsListener : MonoBehaviour
{
    
    public delegate void AttackHandler();
    public delegate void EndAttackHandler();
    public delegate void EndRollHandler();

    public event AttackHandler OnAttack;
    public event EndAttackHandler OnEndAttack;
    public event EndRollHandler OnEndRoll;

    private void HandleAttackContact()
    {
        OnAttack?.Invoke();
    }
    
    private void HandleEndAttack()
    {
        OnEndAttack?.Invoke();
    }
    
    private void HandleEndRoll()
    {
        OnEndRoll?.Invoke();
    }

}
